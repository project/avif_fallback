# AVIF fallback

Creates a fallback jpg image from AVIF images for browsers which do not
support AVIF format.

This module is based on https://www.drupal.org/project/wpf

## Features

- Provide JPEG fallbacks for AVIF images rendered as a responsive image
- Use the JPEG fallback for style urls generated for tokens

## Installation:

You can configure the quality used for the fallback jpeg images
at `/admin/config/media/avif_fallback`

There are two setups to use this module:

### Upload AVIF images

- Install and enable this module
- Use responsive image style in entities display settings for images
- Upload AVIF images to your site

### Convert other image formats to AVIF using image styles

- Install and enable this module
- In image styles add the "Convert" effect and set it to AVIF
- Use responsive image style in entities display settings for images


## Similar modules:

- wpf
- avif
- imageapi optimize avif

## Maintainers

- [Lukas von Blarer](https://www.drupal.org/u/lukas-von-blarer)
