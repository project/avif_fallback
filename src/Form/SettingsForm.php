<?php

namespace Drupal\avif_fallback\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure AVIF fallback image settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wfp_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['avif_fallback.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('avif_fallback.settings');
    $form['quality'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Quality'),
      '#default_value' => $settings->get('quality'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $settings = $this->config('avif_fallback.settings');
    $settings->set('quality', (int) $form_state->getValue('quality'))->save();
    parent::submitForm($form, $form_state);
  }

}
