<?php

namespace Drupal\avif_fallback\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * AVIF fallback image route subscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    foreach (['image.style_public', 'image.style_private'] as $route_id) {
      if ($route = $collection->get($route_id)) {
        $route->setDefault('_controller', 'Drupal\avif_fallback\Controller\ImageStyleDownloadController::deliver');
      }
    }
  }

}
