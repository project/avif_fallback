<?php

namespace Drupal\avif_fallback\Controller;

use Drupal\avif_fallback\ImageFactoryInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\image\Controller\ImageStyleDownloadController as CoreImageStyleDownloadController;
use Drupal\image\ImageStyleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Returns responses for AVIF fallback image.
 */
class ImageStyleDownloadController extends CoreImageStyleDownloadController {

  /**
   * The image quality definition.
   *
   * @var int
   */
  protected $quality;

  /**
   * The ImageFactoryInterface definition.
   *
   * @var \Drupal\avif_fallback\ImageFactoryInterface
   */
  protected $avifFallbackImageFactory;

  /**
   * Constructs a ImageStyleDownloadController object.
   *
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock backend.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   * @param \Drupal\Core\File\FileSystemInterface|null $file_system
   *   The file system.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\avif_fallback\ImageFactoryInterface $avif_fallback_image_factory
   *   The avif_fallback image factory.
   */
  public function __construct(
    LockBackendInterface $lock,
    ImageFactory $image_factory,
    StreamWrapperManagerInterface $stream_wrapper_manager,
    FileSystemInterface $file_system = NULL,
    ConfigFactoryInterface $config_factory,
    ImageFactoryInterface $avif_fallback_image_factory
  ) {
    parent::__construct($lock, $image_factory, $stream_wrapper_manager, $file_system);
    $this->quality = $config_factory->get('avif_fallback.settings')->get('quality') ?? 90;
    $this->avif_fallbackImageFactory = $avif_fallback_image_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('lock'),
      $container->get('image.factory'),
      $container->get('stream_wrapper_manager'),
      $container->get('file_system'),
      $container->get('config.factory'),
      $container->get('avif_fallback.image_factory')
    );
  }

  /**
   * Builds the response.
   */
  public function deliver(Request $request, $scheme, ImageStyleInterface $image_style) {
    // Interesting just the defined in JPEG_PATTERN extensions.
    if (!preg_match(ImageFactoryInterface::JPG_PATTERN, $request->getPathInfo())) {
      // Don't match with extensions go to system image style delivery method.
      return parent::deliver($request, $scheme, $image_style);
    }

    // Prepare image uri.
    $target = $request->query->get('file');
    $image_uri = $scheme . '://';
    $image_uri .= preg_replace('/\\.jpg$/', '', $target);

    // Get the image derivative uri from image style.
    $derivative_uri = $image_style->buildUri($image_uri);
    if (!preg_match('/.avif$/', $derivative_uri)) {
      // The image style is not converted to AVIF format go to system deliver.
      return parent::deliver($request, $scheme, $image_style);
    }

    $headers = [];
    if ($scheme === 'private') {
      // Let other modules provide headers and control access to the file.
      $headers = $this->moduleHandler()->invokeAll('file_download', [$image_uri]);
      if (in_array(-1, $headers, TRUE) || empty($headers)) {
        throw new AccessDeniedHttpException();
      }
    }

    $success = file_exists($derivative_uri);
    $lock_name = 'image_style_deliver:' . $image_style->id() . ':' . Crypt::hashBase64($image_uri);
    if ($success === FALSE) {
      $lock_acquired = $this->lock->acquire($lock_name);
      if (!$lock_acquired) {
        // Retry again in 3 seconds.
        // Currently no browsers are known to support Retry-After.
        throw new ServiceUnavailableHttpException(3, 'Image generation in progress. Try again shortly.');
      }
    }

    // Image style doesn't use AVIF converting don't need creating fallback.
    $success = file_exists($derivative_uri);
    if ($success === FALSE) {
      $success = $image_style->createDerivative($image_uri, $derivative_uri);
    }

    if (!empty($lock_acquired)) {
      $this->lock->release($lock_name);
    }

    if ($success) {
      // We have to prepare the image for request.
      /** @var \Drupal\Core\Image\ImageInterface $avif_image */
      $avif_image = $this->imageFactory->get($derivative_uri);
      if ($this->avif_fallbackImageFactory->createImageCopy($image_uri, $avif_image)) {
        return $this->response(
          $this->avif_fallbackImageFactory->getDestinationUri(),
          $headers, $scheme, $this->avif_fallbackImageFactory->getMimeType());
      }
    }

    // Not generated images log the warning.
    $this->logger->warning('Unable to generate the derived image located at %path.', ['%path' => $derivative_uri]);
    return new Response($this->t('Error generating image.'), 500);
  }

  /**
   * Returns a AVIF image as response.
   *
   * @param string $file
   *   Path to image file.
   * @param array $headers
   *   Response headers.
   * @param string $scheme
   *   The file scheme, defaults to 'private'.
   * @param string $content_type
   *   The acceptable content type.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The transferred file as response.
   */
  protected function response(string $file, array $headers, string $scheme, string $content_type): BinaryFileResponse {
    $headers = array_merge($headers, [
      'Content-Type' => $content_type,
      'Content-Length' => filesize($file),
    ]);

    return new BinaryFileResponse($file, 200, $headers, $scheme !== 'private');
  }

}
